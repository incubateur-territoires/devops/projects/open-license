module "configure-repository-for-deployment" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabk8svariables"
  version = "0.1.0"

  repositories = var.repositories
  scope        = var.gitlab_environment_scope

  cluster_user           = module.namespace.user
  cluster_token          = module.namespace.token
  cluster_namespace      = module.namespace.namespace
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = base64decode(var.kubeconfig.cluster_ca_certificate)

  base-domain = var.base-domain
}

resource "gitlab_project_variable" "helm_upgrade" {
  for_each          = toset([for s in var.repositories : tostring(s)])
  project           = each.value
  environment_scope = var.gitlab_environment_scope
  variable_type     = "file"
  protected         = false

  key   = "HELM_UPGRADE_VALUES"
  value = <<-EOT
  resources:
    limits:
      memory: 1024Mi
    requests:
      cpu: 40m

  ingress:
    enabled: true
    annotations:
      kubernetes.io/ingress.class: haproxy
  podAnnotations:
    monitoring-org-id: ${var.monitoring_org_id}
  service:
    targetPort: 3000
  EOT
}

resource "gitlab_project_variable" "directus_api_url" {
  project           = var.open_license_project_id
  key               = "HELM_ENV_VAR_API_URL"
  value             = var.directus_api_url
  protected         = false
  environment_scope = var.gitlab_environment_scope
}

resource "gitlab_project_variable" "directus_api_token" {
  project           = var.open_license_project_id
  key               = "HELM_ENV_VAR_API_TOKEN"
  value             = var.directus_api_token
  protected         = false
  environment_scope = var.gitlab_environment_scope
}
