locals {
  repositories = [data.gitlab_project.open_license.id]
}

module "reviews" {
  source                   = "./environment"
  gitlab_environment_scope = "*"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base-domain              = var.dev_base-domain
  namespace                = "${var.project_slug}-reviews"
  repositories             = local.repositories
  open_license_project_id  = data.gitlab_project.open_license.id
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_cpu    = 2
  namespace_quota_max_memory = "12Gi"

  monitoring_org_id = random_string.development_secret_org_id.result

  directus_api_url   = var.directus_api_url
  directus_api_token = var.directus_api_token
}

module "development" {
  source                   = "./environment"
  gitlab_environment_scope = "development"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base-domain              = var.dev_base-domain
  namespace                = "${var.project_slug}-development"
  repositories             = local.repositories
  open_license_project_id  = data.gitlab_project.open_license.id
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_cpu    = 2
  namespace_quota_max_memory = "12Gi"

  monitoring_org_id = random_string.development_secret_org_id.result

  directus_api_url   = var.directus_api_url
  directus_api_token = var.directus_api_token
}
